package com.edd.test;

public class Alarm {

    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;

    private Sensor sensor;

    private boolean alarmOn;

    public Alarm() {

        this.sensor = new Sensor();
        this.alarmOn = false;

    }

    public void check() {

        double psiPressureValue = this.sensor.popNextPressurePsiValue();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {

            this.alarmOn = true;

        }

    }

    public boolean isAlarmOn() {

        return this.alarmOn;

    }

}
