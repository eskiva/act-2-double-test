package com.edd.test;

import org.junit.*;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class TestAlarm {

    /**
     * Comprobar que la alarma está desactivada por defecto
     */
    @Test
    public void testAlarmIsNotOnByDefault() {

        Alarm alarm = Mockito.mock(Alarm.class);
        when(alarm.isAlarmOn()).thenReturn(false);

        Sensor sensor = new Sensor();
        Alarm alarm1 = new Alarm(sensor, false);
        Assertions.assertEquals(false, alarm1.isAlarmOn(), "testAlarmIsNotOnByDefault");

    }
    /**
     * Comprobar que la alarma se activa con bajas presiones
     */
    @Test
    public void testAlarmOnWithLowPressure() {

        Sensor sensor = new Sensor();
        Alarm alarm = Mockito.mock(Alarm.class);
        sensor.popNextPressurePsiValue();
        when(alarm.isAlarmOn()).thenReturn(true);

        Sensor sensor1 = new Sensor();
        Alarm alarm1 = new Alarm(sensor1, false);
        alarm1.check();
        Assertions.assertEquals(true, alarm1.isAlarmOn(), "testAlarmOnWithHighPressure");

    }

    /**
     * Comprobar que la alarma se activa con altas presiones
     */
    @Test
    public void testAlarmOnWithHighPressure() {

        Sensor sensor = new Sensor();
        Alarm alarm = Mockito.mock(Alarm.class);
        sensor.popNextPressurePsiValue();
        when(alarm.isAlarmOn()).thenReturn(true);

        Sensor sensor1 = new Sensor();
        Alarm alarm1 = new Alarm(sensor1, false);
        sensor1.popNextPressurePsiValue();
        Assertions.assertEquals(true, alarm1.isAlarmOn(), "testAlarmOnWithHighPressure");

    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones normales
     */
    @Test
    public void testAlarmOffWithNormalPressure() {


    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones límite
     */
    @Test
    public void testAlarmOffWithLimitsPressure() {


    }

    /**
     * Comprobar que la alarma se activa con presiones límite por encima y por debajo
     */
    @Test
    public void testAlarmOnWithLimitsPressure() {


    }
    
}
